Night mode for GNOME! Automatically toggle your light and dark GTK and GNOME Shell theme variants, switch backgrounds and launch custom commands at sunset and sunrise.

Supports Night Light, Location Services and manual schedule.

It works out of the box with numerous themes (see the list on the repository), and you can manually choose the variants you want.
